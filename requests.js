// SETTING UP FUNCTIONS TO MAKE HTTP REQUEST
// USING FETCH API.
const getPuzzleFetch = (wordCount) => {
    return fetch(`http://puzzle.mead.io/puzzle?wordCount=${wordCount}`, {}).then((response) => {
        if (response.status === 200) {
            return response.json()
        } else {
            throw new Error('This request failed hard!')
        }
    }).then((data) => {
        return data.puzzle;
    })
}

const getCountryFetch = (countryCode) => {
    return fetch('http://restcountries.eu/rest/v2/all', {}).then((response) => {
        if (response.status === 200) {
            return response.json()
        }
    }).then((data) => {
        const country = data.find((c) => c.alpha2Code === countryCode)
        return country;
    }).catch((error) => {
        return 'This is NOT the way!';
    })
}

const getLocation = () => {
    return fetch('http://ipinfo.io/json?token=9922d983c6dcbb', {}).then((response) => {
        if (response.status === 200) {
            return response.json()
        } else {
            throw new Error('This request failed hard!')
        }
    })
}