const puzzleEl = document.querySelector('#puzzle')
const guessesEl = document.querySelector('#guesses')
const game1 = new Hangman('Car Parts', 2)

puzzleEl.textContent = game1.puzzle
guessesEl.textContent = game1.statusMessage

window.addEventListener('keypress', (e) => {
    const guess = String.fromCharCode(e.charCode)
    game1.makeGuess(guess)
    puzzleEl.textContent = game1.puzzle
    guessesEl.textContent = game1.statusMessage
});


// BELOW IS ARE FUNCTION CALLS TO FUNCTIONS LOCATED IN
// REQUESTS.JS. FUNCTIONS UTILIZE THE FETCH API FOR HTTP REQUESTS.
getPuzzleFetch(3).then((data) => {
    console.log(data)
}).catch((error) => {
    console.log('Big time failure')
})


// Challenge Time!
// 1. Create getLocation function which takes no arguments.
// 2. Setup getLocation to make a request to the url and parse the data.
// 3 Use getLocation to print the city, region and country information.
// Print a message: "You are currently in city, region, country"

getLocation().then((data) => {
    return getCountryFetch(data.country)
}).then((country) => {
    console.log(country.name)
}).catch((error) => {
    console.log('Big time failure:', error)
})