// Promise
const getDataPromise = (num) => new Promise((resolve, reject) => {
    setTimeout(() => {
        typeof num === 'number' ? resolve(num * 2) : reject('Not a number')
    }, 2000)
})

// BAD APPROACH TO DOING MULTIPLE THINGS WITH DATA THAT IS BEING RETURNED
// BACK VIA A PROMISE
getDataPromise(2).then((data) => {
    getDataPromise(data).then((data) => {
        console.log('Inside promise data after multiplication:', data)
    }, (error) => {
        console.log('First promise error:', error)
    })
    console.log('Outside promise data before multiplication:', data)
}, (error) => {
    console.log(error)
})

// PROPER APPROACH TO DOING MULTIPLE THINGS WITH DATA THAT IS BEING RETURNED
// BACK VIA A PROMISE. THIS APPROACH IS KNOWN AS "PROMISE CHAINING"
getDataPromise(10).then((data) => {
    return getDataPromise(data)
}).then((data) => {
    return 'This is some test data'
}).then((data) => {
    console.log(data)
}).catch((error) => {
    console.log(error)
})